{-# LANGUAGE OverloadedStrings #-}

{-|
Module       : Main
Description  : Creates scotty app to update and display projects
Copyright    : (c) Philip Woods 2015
License      : AGPL-3
Maintainer   : elzairthesorcerer@gmail.com
Stability    : experimental
Portabiltity : Linux
-}

module Main where

import Control.Monad.IO.Class
import Control.HannahCI.Handler
import Data.Info
import Data.Text.Lazy (pack)
import Network.HTTP.Types.Status
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Static
import Web.Scotty
import Web.Scotty.Internal.Types

-- | Main function
main :: IO ()                           -- ^ No return value
main = scotty 3000 $ do
  middleware $ staticPolicy (noDots >-> addBase (dataPath ++ "static"))
  middleware logStdoutDev
  get "/"         $ file "/usr/share/hannahci/static/index.html"
  get "/projects" showProjects
  post "/"        update

-- | Return JSON formatted list of the metadata for all projects
--   in config file
showProjects :: ActionM ()              -- ^ Scotty action 
showProjects = do
  repos <- liftIO $ getAllRepos
  case repos of
    Left err -> do
      status status500
      json $ pack err
    Right info -> json info

-- | Process JSON request from Provider webhook
update :: ActionM ()                    -- ^ Scotty action
update = do
  b <- body
  liftIO $ processUpdate b
  status ok200
