#!/bin/bash -e

ARCHITECTURE=amd64
VERSION=$(grep -e '^version' hannahci.cabal | awk '{print $2}')
DEBPKGVER=1
DEBVER=$VERSION-$DEBPKGVER
BASE=hannahci-$DEBVER-$ARCHITECTURE
DIST=`pwd`/$BASE
DEST=$DIST/usr
ME=$(whoami)
COPYRIGHT=$DEST/share/hannahci/LICENSE

rm -rf $DIST

echo Initializing sandbox...
cabal sandbox init
echo Updating cabal pacakges
cabal update

export PATH=`pwd`/.cabal-sandbox/bin:$PATH
echo Building hannahci...
cabal clean
cabal configure
cabal build

strip dist/build/hannahci/hannahci
mkdir -p $DIST/etc/init
mkdir -p $DEST/bin
mkdir -p $DEST/share/hannahci/static
cp conf/hannahci.yaml  $DIST/etc/hannahci.yaml
cp init/hannahci.conf  $DIST/etc/init/hannahci.conf
cp dist/build/hannahci/hannahci $DEST/bin/
cp static/* $DEST/share/hannahci/static/

mkdir $DIST/DEBIAN
INSTALLED_SIZE=$(du -B 1024 -s $DEST | awk '{print $1}')
cp init/setup-local.sh $DIST/DEBIAN/postinst
perl -pe "s/VERSION/$DEBVER/" deb/control.in | \
    perl -pe "s/ARCHITECTURE/$ARCHITECTURE/" | \
    perl -pe "s/INSTALLED_SIZE/$INSTALLED_SIZE/" \
         > $DIST/DEBIAN/control

fakeroot dpkg-deb --build $DIST
rm -rf $DIST
