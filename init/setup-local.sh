#!/bin/sh

# Add user
useradd -m -d /home/hannahci -s /bin/false hannahci

# Give user non-sudo access to docker
gpasswd -a hannahci docker
service docker restart

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# Add SSH Keys
SSHPATH="/home/hannahci/.ssh"
sudo -u hannahci mkdir -p /home/hannahci/.ssh
sudo -u hannahci chmod 700 /home/hannahci/.ssh
sudo -u hannahci ssh-keygen -t rsa -N "" -C "hannahci" -f "$SSHPATH/id_rsa"
sudo -u hannahci cat "$SSHPATH/id_rsa.pub"
echo "Add the previous line to the list of SSH keys for your git host account."

