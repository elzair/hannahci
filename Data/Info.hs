{-# LANGUAGE DeriveGeneric #-}

{-|
Module       : Data.Info
Description  : Types and variables used throughout the project
Copyright    : (c) Philip Woods 2015
License      : AGPL-3
Maintainer   : elzairthesorcerer@gmail.com
Stability    : experimental
Portabiltity : Linux
-}

module Data.Info (
  ProjectInfo(..),
  HostInfo(..),
  LogInfo(..),
  pathRoot,
  tPathRoot,
  confPath,
  dataPath
) where

import Data.Aeson   (FromJSON(..), ToJSON(..))
import Data.Text    (Text, pack)
import GHC.Generics (Generic)

-- | Information on given project
data ProjectInfo = ProjectInfo {   
  provider    :: Text,             -- ^ Provider name (i.e. bitbucket)
  repository  :: Text,             -- ^ Repository name
  branch      :: Text,             -- ^ Branch name
  preCommands :: Maybe [Text],     -- ^ List of commands to execute before building container
  hostInfo    :: Maybe [HostInfo]  -- ^ List of host-specific configuration
} deriving (Show, Generic)

instance FromJSON ProjectInfo
instance ToJSON   ProjectInfo

-- Two 'ProjectInfo' variables are equivalent if they have the
-- same provider, repository, and branch name.
instance Eq ProjectInfo where
  (ProjectInfo p1 r1 b1 _ _) == (ProjectInfo p2 r2 b2 _ _) =
    p1 == p2 && r1 == r2 && b1 == b2
 
-- | Information on application host
data HostInfo = HostInfo {
  hostName   :: Text,              -- ^ Name of application host
  runOptions :: Maybe Text         -- ^ Options to pass to Docker
} deriving (Show, Generic)

instance FromJSON HostInfo
instance ToJSON   HostInfo

-- | Contents and metadata of log file
data LogInfo = LogInfo {
  provider_info   :: Text,         -- ^ Provider name
  repository_info :: Text,         -- ^ Repository name
  branch_info     :: Text,         -- ^ Branch name
  log_info        :: Text          -- ^ Contents of latest log file
} deriving (Show, Generic)

instance FromJSON LogInfo
instance ToJSON   LogInfo

-- | Path to directory where data is stored
pathRoot :: String
pathRoot = "/home/hannahci/"

-- | The 'Text' version of 'pathRoot'
tPathRoot :: Text
tPathRoot = pack "/home/hannahci/"

-- | The path to the config file
confPath :: String
confPath = "/etc/hannahci.yaml"

-- | The path to the data directory
dataPath = "/usr/share/hannahci/"
