#!/bin/sh

ssh "$1" sudo useradd -m -s /bin/false -d /home/hannaci hannahci.conf
ssh "$1" sudo gpasswd -a hannahci docker
ssh "$1" sudo service docker restart
ssh "$1" sudo -u hannahci mkdir /home/hannahci/.ssh
sudo -u hannahci cp /home/hannahci/.ssh/id_rsa.pub /tmp/
sudo -u hannahci chmod 777 /tmp/id_rsa.pub
scp /tmp/id_rsa.pub "$1:/tmp/id_rsa.pub"
ssh "$1" sudo -u hannahci cp /tmp/id_rsa.pub /home/hannahci/.ssh/authorized_keys
ssh "$1" sudo -u hannahci chmod 700 /home/hannahci/.ssh
